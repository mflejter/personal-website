import { Component, ElementRef, HostListener } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-experience-page',
  templateUrl: './experience-page.component.html',
  styleUrls: ['./experience-page.component.scss'],
  animations: [
    trigger('scrollAnimation', [
      state('show', style({
        opacity: 1,
      })),
      state('hide', style({
        opacity: 0,
      })),
      transition('show => hide', animate('250ms ease-out')),
      transition('hide => show', animate('250ms ease-in'))
    ])
  ]
})
export class ExperiencePageComponent {


  // Chart options
  gradient = true;
  showLegend = false;
  colorScheme = {
    name: 'cool',
    selectable: true,
    group: 'Ordinal',
    domain: [
      '#a8385d', '#7aa3e5', '#a27ea8', '#aae3f5', '#adcded', '#a95963', '#8796c0', '#7ed3ed', '#50abcc', '#ad6886'
    ]
  };
  labels = true;
  view: any[] = [700, 400];

  // Skills data for the GE Software Engineer position
  geSWEData: any[] = [
    {
      'name': 'Java',
      'value': 65,
    },
    {
      'name': 'Angular 2+',
      'value': 50,
    },
    {
      'name': 'AngularJS',
      'value': 25,
    },
    {
      'name': 'JUNIT',
      'value': 20,
    },
    {
      'name': 'Swing',
      'value': 15,
    },
    {
      'name': 'SQL',
      'value': 10,
    },
    {
      'name': 'Drools',
      'value': 30,
    },
    {
      'name': 'Jasmine',
      'value': 20,
    },
    {
      'name': 'Karma',
      'value': 20,
    },
    {
      'name': 'Selenium',
      'value': 20,
    },
    {
      'name': 'Cucumber',
      'value': 20,
    },
    {
      'name': 'Jenkins',
      'value': 10,
    },
    {
      'name': 'Git/GitLab',
      'value': 30,
    },
    {
      'name': 'CSS/SCSS',
      'value': 30,
    },
    {
      'name': 'Bootstrap',
      'value': 15,
    },
    {
      'name': 'Linux OS',
      'value': 40,
    },
    {
      'name': 'Rally',
      'value': 30,
    },
    {
      'name': 'Agile Methodology',
      'value': 30,
    }
  ];

  // Skills data for the GE internship position
  geInternData: any[] = [
    {
      'name': 'Java',
      'value': 40,
    },
    {
      'name': 'Angular 2+',
      'value': 65,
    },
    {
      'name': 'JUNIT',
      'value': 20,
    },
    {
      'name': 'Jasmine',
      'value': 30,
    },
    {
      'name': 'Karma',
      'value': 30,
    },
    {
      'name': 'Selenium',
      'value': 20,
    },
    {
      'name': 'Cucumber',
      'value': 20,
    },
    {
      'name': 'Git/GitLab',
      'value': 30,
    },
    {
      'name': 'CSS/SCSS',
      'value': 30,
    },
    {
      'name': 'Bootstrap',
      'value': 15,
    },
    {
      'name': 'Linux OS',
      'value': 40,
    },
    {
      'name': 'Rally',
      'value': 30,
    },
    {
      'name': 'Agile Methodology',
      'value': 30,
    }
  ];

  // Skills data for the northwestern mutual internship position
  nmInternData: any[] = [
    {
      'name': 'Microsoft Office 365 Administration',
      'value': 75,
    },
    {
      'name': 'PowerShell Scripting',
      'value': 75,
    },
    {
      'name': 'Java',
      'value': 15,
    },
    {
      'name': 'HTML',
      'value': 15,
    },
    {
      'name': 'CSS',
      'value': 15,
    },
    {
      'name': 'Jira',
      'value': 15,
    },
    {
      'name': 'AgileCraft',
      'value': 15,
    },
    {
      'name': 'Git',
      'value': 15,
    },
    {
      'name': 'Agile Methodology',
      'value': 30,
    }
  ];

  /**
   * Determines if animated elements should be hidden or not
   */
  state = 'hide';

  /**
   * Holds the value of the currently selected nav item
   */
  currentNavItem = 'GEEngineer';

  /**
   * Holds whether the skills or description section is selected
   */
  selectedSection = 'description';

  /**
   * Constructor
   */
  constructor(public el: ElementRef) { }

  /**
   * Listens for scroll events and sets the state of the animated elements
   */
  @HostListener('window:scroll', ['$event'])
  checkScroll() {
    const componentPosition = this.el.nativeElement.offsetTop / 1.25;
    const scrollPosition = window.pageYOffset;
    if (scrollPosition >= componentPosition) {
      this.state = 'show';
    } else {
      this.state = 'hide';
    }
  }

  /**
   * Gets called everytime an item in the navbar is clicked
   */
  handleNavClick(item: string) {
    this.currentNavItem = item;
    this.selectedSection = 'description';
  }

  /**
   * Gets called everytime a radio button is clicked
   */
  handleRadioClick(button: string) {
    this.selectedSection = button;
  }

  /**
   * Formats the tooltip for the pie charts; only returns the name of the category, not the value
   */
  formatTooltip($event) {
    return $event.data.name;
  }
}
