import { Component } from '@angular/core';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  /**
   * Keeps track of the currently clicked page
   */
  activePage = 'home';

  /**
   * Determines if the main content of the page should be displayed
   */
  displayMainContent = false;

  /**
   * Class constructor
   * @param meta
   */
  constructor(private meta: Meta) {
    this.meta.addTag({ name: 'description', content: 'Milwaukee, Wisconsin based Software Engineer' });
    this.meta.addTag({ name: 'author', content: 'Matthew Flejter' });
    this.meta.addTag({ name: 'keywords', content: 'angular, react, java, javascript, software, engineer, developer, wisconsin, waukesha, pewaukee, milwaukee, full stack eengineer, front-end engineer, back-end engineer' });
    this.meta.addTag({ name: 'title', content: 'Matthew Flejter | Software Engineer' });
    this.meta.addTag({ name: 'og:description', content: 'Milwaukee, Wisconsin based Software Engineer' });
    this.meta.addTag({ name: 'og:author', content: 'Matthew Flejter' });
    this.meta.addTag({ name: 'og:keywords', content: 'angular, react, java, javascript, software, engineer, developer, wisconsin, waukesha, pewaukee, milwaukee, full stack eengineer, front-end engineer, back-end engineer' });
    this.meta.addTag({ name: 'og:title', content: 'Matthew Flejter | Software Engineer' });
  }

  /**
   * Sets the nav item to be selected and scrolls the selected page into view
   */
  handleItemClick(clickedItem: string) {
    this.activePage = clickedItem;
  }

  /**
   * Gets called when the terminal component is done processing
   */
  handleDoneProcessing() {
    this.displayMainContent = true;
  }
}
