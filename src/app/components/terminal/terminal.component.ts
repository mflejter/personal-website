import { Component, Inject, OnInit, EventEmitter, Output } from '@angular/core';
import { Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-terminal',
  templateUrl: './terminal.component.html',
  styleUrls: ['./terminal.component.scss']
})
export class TerminalComponent implements OnInit {
  /**
   * Determines if the terminal should be show or not
   */
  show = false;

  /**
   * Emits an event when the terminal should not be shown anymore/is done processing
   */
  @Output() doneProcessing: EventEmitter<any> = new EventEmitter<any>();

  /**
   * Constructor
   */
  constructor(@Inject(DOCUMENT) private document, private renderer2: Renderer2) { }

  /**
   * Janky way of appending a script tag that is required for termynal.js
   */
  ngOnInit() {
    const s = this.renderer2.createElement('script');
    s.type = 'text/javascript';
    s.src = '/assets/termynal/termynal.js';
    s.text = ``;
    s['data-termynal-container'] = '#termynal';
    this.renderer2.setAttribute(s, 'data-termynal-container', '#termynal');
    this.renderer2.appendChild(this.document.body, s);
    this.show = true;

    setTimeout(() => {
      this.doneProcessing.emit();
    }, 7700);
  }
}
